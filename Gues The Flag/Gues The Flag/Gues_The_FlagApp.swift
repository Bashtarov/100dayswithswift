//
//  Gues_The_FlagApp.swift
//  Gues The Flag
//
//  Created by Мухьаммад on 06.12.2022.
//

import SwiftUI

@main
struct Gues_The_FlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
