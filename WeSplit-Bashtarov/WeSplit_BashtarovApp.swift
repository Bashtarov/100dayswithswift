//
//  WeSplit_BashtarovApp.swift
//  WeSplit-Bashtarov
//
//  Created by Мухьаммад on 24.11.2022.
//

import SwiftUI

@main
struct WeSplit_BashtarovApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
