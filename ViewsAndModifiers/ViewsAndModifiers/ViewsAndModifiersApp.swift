//
//  ViewsAndModifiersApp.swift
//  ViewsAndModifiers
//
//  Created by Мухьаммад on 11.12.2022.
//

import SwiftUI

@main
struct ViewsAndModifiersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
